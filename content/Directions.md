# Location & directions

## Directions to the Library: Public Transport
The symposium will take place at the TU Delft Library (Google maps link: [TU Delft Library](https://maps.app.goo.gl/VBJLQnK5dy1wD5LP6))
```{figure} ./images/Library.png
---
scale: 40%
align: right
---
```
- Take the train to **Delft NS Station**
- Take one of the following buses:
  - **455** (to Zoetermeer Centrum West) to *Stop Christiaan Huygensweg, Delft* - Walk 4 minutes.
  - **174** (to Rotterdam Noord via Berkel) to *Stop Aula TU, Delft* - Walk 3 minutes.
  - **69** (to TU Campus) to *Stop Christiaan Huygensweg, Delft* - Walk 4 minutes.
  - **40** (to Rotterdam Centraal) to *Stop Jaffalaan, Delft* - Walk 9 minutes.


## Directions to the Library: Car
- Follow the A13 motorway. Take exit 10 Delft Zuid/TU-wijk.
- Parking spot nearest to the library: [P1 Parking Spot](https://www.google.com/maps/dir//Van+der+Waalsweg,+2628+CN+Delft/@52.0014014,4.3730794,437m/data=!3m1!1e3!4m9!4m8!1m0!1m5!1m1!1s0x47c5b58d750a9ec3:0x753a86dd18c9afff!2m2!1d4.3752681!2d52.0013981!3e0)
- Visitor parking rate: **€2 per hour** (maximum of **€10 per day**).

```{figure} ./images/ParkingSpace.png
```

More information can be found here: [TU Delft Accessibility](https://www.tudelftcampus.nl/accessibility/)

## Map of the library building:
```{figure} ./images/Layout.png
```
