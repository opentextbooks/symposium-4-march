# Programme 

**12.30 – 13.00** Walk-in, registration, lunch   
 
## Plenary 
**13.10 - 13.20** Welcome and opening - Irene Haslinger (Library Director)    
**13.20 - 14.00** Keynote – [Wilma van Wezenbeek](Wilma)   
**14.00 - 14.20** Project results - [Timon Idema](Timon)   
**14.20 - 14.50** Panel discussion: "It adds value for students when books are openly published."    
**14.50 - 15.10** Book presentation - [Timon Idema](Timon)     
**15.10 - 15.30** Book presentation - [Robert Lanzafame](Robert) & [Tom van Woudenberg](Tom)   
**15.30 - 16.00** Break   
 
## Breakouts 
**16.00 - 16.45** Workshops and presentations:
- How to make a Jupyter Book - Timon Idema
- Swimming in the “Copyright lake” - Paschalis Kontanas & Jacqueline Michielen
- How students used an OIT. Results of a survey. - Sander Haemers
- Teaching computational design skills at IDE - Willemijn Elkhuizen
- Students choosing their learning experience with open education materials - Wim Bouwman
- The Jupyter Book Publication Process at TU Delft - Thom Groen
- Creating student-activating books: don’t forget the teachers! -  Robert Lanzafame & Tom van Woudenberg
- Use of open interactive textbooks and materials in education - Muriël van den Berg & Nancy de Groot
- From static to interactive - Aurèle Adam

For more information on the content of the breakout sessions see the [breakout programme](./Breakout.md).
 
## Plenary 
**16.50 - 17.00** Closing remarks  
**17.00- 17.30** Drinks  
