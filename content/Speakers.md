# Speakers

(Wilma)=
## Wilma van Wezenbeek (Keynote speaker)

```{figure} ./images/WilmaVanWezenbeek.png
---
scale: 15%
align: right
---
```
Wilma van Wezenbeek is a seasoned professional renowned for her executive experience and expertise in digital transformation, scientific publishing, and research library management. Specializing in open access and education, she excels as a networker, leader, and practical solution finder. Since 2023, she leads tshe Transformation Hub for Digital Learning Materials within the [NPuls](https://npuls.nl/) program, collaborating with Lieke Rensink and Menno de Waal to enhance the accessibility and reusability of educational resources. 


(Timon)= 
## Timon Idema
```{figure} ./images/TimonIdema.png
---
scale: 15%
align: right
---
```
Timon Idema, associate professor at Delft University of Technology, is a renowned educator and researcher in the field of Bionanoscience. Having earned his PhD in theoretical biophysics from Leiden University, Idema has a distinguished international background, having worked at Institut Curie in Paris and the University of Pennsylvania in Philadelphia. His research group focuses on collective dynamics in biologically motivated systems, exploring scales from nano to macro. As a theorist, Idema collaborates extensively with experimental groups and imparts cutting-edge knowledge to students through various courses. Since 2020, he has led the joint-degree MSc Nanobiology program at TU Delft and Erasmus MC Rotterdam, receiving accolades for his educational contributions, including an Open Education Ambassador Award and the [SURF Onderwijsaward](https://www.surf.nl/winnaars-surf-onderwijsawards-2023) in 2023. At the symposium he will present his open interactive book [Introduction to particle and continuum mechanics](https://interactivetextbooks.tudelft.nl/nb1140/).

(Robert)= 
## Robert Lanzafame
```{figure} ./images/RobertLanzafame.png
---
scale: 15%
align: right
---
```
Robert Lanzafame, a Civil Engineer with a PhD from UC Berkeley, has a background in geotechnical engineering, probabilistic design, and engineering geology. As a Senior Lecturer, he actively contributes to education, teaching courses from fundamental engineering methods to applied levee design. His current passion is integrating computational thinking into education, using open-source tools like Jupyter Books, and making it easier for colleagues to do the same.

Robert manages the Modelling, Uncertainty, and Data for Engineers (MUDE) module, crucial for first-year MSc students in the Civil Engineering and Geosciences faculty. Additionally, he teaches applied structural reliability for students in the Hydraulic and Offshore Structures track. Reach out if you share his enthusiasm for the intersection of technology and education!

(Tom)=
## Tom van Woudenberg
```{figure} ./images/TomVanWoudenberg.png
---
scale: 15%
align: right
---
```
Tom is a lecturer in structural mechanics at the Faculty of Civil Engineering and Geosciences. He has a passion for mechanics education, and he strives to facilitate a blended learning approach for students, making active learning attractive and rewarding. Tom graduated from TU Delft in 2020 with research focusing on mathematical optimization of structural design. From August 2020 to August 2022, he worked at the Amsterdam University of Applied Sciences as a construction lecturer, specializing in structural mechanics. Since September 2022, Tom has been employed at TU Delft.