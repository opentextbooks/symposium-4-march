# Registration

Please register for the symposium via the link below:

> [**Registration form**](https://forms.office.com/Pages/ResponsePage.aspx?id=TVJuCSlpMECM04q0LeCIe30g6N5KbJdNhA-JRp_LU2NUQzhBSUMzQTQ1NkFDRFpXQTM5U0EzVzAwVS4u)

```{note}
Registration is possible until March 1st. Please be aware that we cannot guarantee lunch to be available when you sign up after February 27th.
```