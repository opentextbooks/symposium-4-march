# Breakout programme 

## How to make a Jupyter Book
**Timon Idema**

Jupyter Books are created from pages written in MarkDown or Jupyter Notebooks. In this session, we’ll help you get started with MarkDown, which is easy to learn, and show you how to set up your first Jupyter Book. The book can be created from scratch or based on material you already have. 
```{note}
To be able to participate, please bring a laptop.
```

## Swimming in the “Copyright lake”
**Paschalis Kontanas & Jacqueline Michielen**

A brief introduction to copyright and copyright rules. An explanation of the Creative Commons licenses, especially CC BY, and how to combine these licenses. How to create and reuse materials for open use: which materials can be used, and where and how can they be found?

## How students used an OIT. Results of a survey.
**Sander Haemers**

The OIT for the course Electricity and Magnetism is supplementary to the Textbook. It contains instructional videos and examples of Python code to calculate equations or plot functions. We already knew that the OIT is highly appreciated, but we thought it would be interesting to ask in a survey how and for what purpose the students used this OIT. In the session, we will discuss the results of this survey.

## Teaching computational design skills at IDE
**Willemijn Elkhuizen**

In this session, Willemijn will present our process and outcomes of transforming conventional lecture/tutorial-based instruction on computational design (using Rhino Grasshopper) into an open, interactive textbook, suitable for self-paced and autonomous learning, showing comparable learning satisfaction and outcomes.

## Students choosing their learning experience with open education materials
**Wim Bouwman**

Electricity and Magnetism is a 1st-year course in the BSc in Applied Physics at TU Delft. For this course, we created an open, interactive textbook consisting of short videos with descriptive texts and python calculations as illustrations. In this presentation, we describe how we arrived at this textbook, what its content is and how we now use it in our teaching.

## The Jupyter Book Publication Process at TU Delft
**Thom Groen**

In this session Thom Groen (Open Interactive Textbooks project co-lead) will walk you through the publication process of publishing your Jupyter Book through the TU Delft library. He will share best practices, what to look out for and show off the guiding materials the project team has created in order to make this process as smooth as possible.

## Creating student-activating books: don’t forget the teachers!
**Robert Lanzafame & Tom van Woudenberg (CEG faculty)**

Robert and Tom draw on recent experience making student-activating textbooks: with teams from 1 to 20 contributors! Their philosophy is to make it possible, practical and fun for all teachers, regardless of experience. The presentation will focus on design and implementation of teaching materials, and an innovative feature supporting Python computation in a web-browser---immediately! Time is reserved for a discussion and small demonstration: you might even have your own online book by the end!

## Use of open interactive textbooks and materials in education – design & practice
**Muriël van den Berg & Nancy de Groot**

This session will dive into (1) the storyboard as a design method to make a deliberate choice on how to integrate an open interactive textbook in your course, and (2) several examples of open educational resources as implemented in the quantum information science & technology (QIST) master programme.

## From static to interactive
**Aurèle Adam**

We would like to discuss about the challenges to move from a static book to an interactive book.