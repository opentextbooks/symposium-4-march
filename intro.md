# Education: open & interactive!
## Symposium 4 March
Join us at the symposium on March 4th. Discover the latest advancements in Jupyter Books, connect with the Open Interactive Textbooks (OIT) community that has grown within TU Delft and celebrate the authors who have published [Open Interactive Textbooks](https://textbooks.open.tudelft.nl/textbooks/catalog/category/interactive) in the past year. The OIT project team is excited to reunite with familiar faces and welcome members of our community, as well as new faces from both TU Delft and other institutions. Read more about the [programme](./content/Programme.md) and [register](https://forms.office.com/Pages/ResponsePage.aspx?id=TVJuCSlpMECM04q0LeCIe30g6N5KbJdNhA-JRp_LU2NUQzhBSUMzQTQ1NkFDRFpXQTM5U0EzVzAwVS4u).

Warm regards,

Carola van der Muren & Thom Groen, Project Leads


```{figure} ./content/images/Homepage_image.png
```